T = [23 93 293];
%
B0 = 1;
Jmax = 100;
JmaxPlot = 20;
N = 150;
%
J = [0:Jmax];
n = length(J);
E = B0*J.*(J+1);
xp = [0 1];
Ep = [E' E'];
%
Constantes;
%
figure;
%
it = 0;
PP = [];
for TT = T
it = it+1;
%
p = (2*J+1).*exp(-E*h*c*100/k/TT);
p = p/sum(p);
%
subplot(1,length(T),it);
%
hold on
%
for i = 1:n
%
plot(xp,Ep(i,:),'b-');
nJ = round(N*p(i));
for j = 1:nJ;
  plot(5*j/N,Ep(i),'r.','Markersize',8);
end
%
end
%
hold off
%
set(gca,'Box','on');
set(gca,'XLim',[-0.01 1.05]);
set(gca,'XTick',[]);
set(gca,'YLim',[-5 JmaxPlot*(JmaxPlot+1)+5]);
%
if(it == 1)
xlabel('Population');
ylabel('Énergie / Unité arbitraire');
end
title(['{\itT} = ' num2str(TT-273) ' °C'],'FontWeight','normal');
%
end
%
print -dpng FigPopBoltzmann_FR;
