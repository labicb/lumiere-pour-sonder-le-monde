i = [-90:0.001:0];
L = [700:-1:400];
f = pi/180;
ir = i*f;
%
mx = [];
%
pkg load image
%
figure;
%
hold on
%
il = 0;
for l = L
il = il+1;
A = (pi+6*asin(sin(ir)/nwater(l))-2*ir)/f;
mx(il) = min(A);
col = wavelength2rgb(l);
plot(i,A,'-','Color',col);
%
end
hold off
%
set(gca,'Box','on');
xlabel('Incidence / °');
ylabel('Déviation / °');
set(gca,'XLim',[-90,-30]);
set(gca,'YLim',[40,90]);
grid on;
print -dpng Fig_deviation2_exacte;
%
figure;
%
plot(L,mx,'r-');
%
set(gca,'Box','on');
xlabel('\lambda / nm');
ylabel('Déviation maximale / °');
grid on;
print -dpng Fig_deviation2_min;
