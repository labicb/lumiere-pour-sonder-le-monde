%
% Voir : http://culturesciencesphysique.ens-lyon.fr/ressource/arcenciel.xml
%
X = [-1:0.001:1];
Y = sqrt(1-X.^2);
Z = zeros(size(X));
L = [400:5:700];
mx = 0.75;
%
pkg load geometry
pkg load image
%
figure('PaperUnits','centimeters','PaperPosition',[1 1 27.7 19],'PaperOrientation','landscape');
view([-38 8]);
hold on
%
for l = L
%
n = nwater(l);
i = acos(sqrt(((n^2-1)/3)));
A = 4*asin(sin(i)/n)-2*i;
r = sin(A);
x = X*r;
y = Y*r;
z = Z;
col = wavelength2rgb(l);
%
plot3(z,x,y,'-','Color',col);
%
end
%
for l = [max(L) min(L)];
n = nwater(l);
i = acos(sqrt(((n^2-1)/3)));
A = 4*asin(sin(i)/n)-2*i;
ray = (X+1)*sin(A);
col = wavelength2rgb(l);
ind = find(X <= 0.3);
plot3(X(ind),Z(ind),ray(ind),'-','Color',col);
end
%
hold off
%
xlabel('x');
ylabel('y');
zlabel('z');
set(gca,'Box','on');
set(gca,'Xlim',[-1 1]);
set(gca,'YLim',[-mx mx]);
set(gca,'ZLim',[0 mx*1.2]);
set(gca,'YDir','reverse');
set(gca,'BoxStyle','full');
grid on;
set(gca,'Visible','off');
%
print -dpdf Fig_arc

