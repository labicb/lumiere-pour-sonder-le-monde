drawnorm = 0;
ax = 0;
atneg = 0;
colray = 1;
colgoutte = 1;
%L = [440 510 650];
L = [700:-20:400];
%L = [700:-5:400];
%L = 650;
%L = [400 700];
%angle = [0:1:90];
%angle = [-50 50];
angle = 60;
%angle = 43
%angle = [-40:-1:-90];
%angle = -72;
%angle = -55;
%
%lw = 0.1;
lw = 1;
%
cw = 1;
%
%lnorm = 2;
lnorm = 1.2;
%
fa = pi/180;
%
pkg load geometry
pkg load image
%
f1 = figure('PaperUnits','centimeters','PaperPosition',[1 1 19 19]);
hold on
f2 = figure('PaperUnits','centimeters','PaperPosition',[1 1 19 19]);
hold on
%
figure(f1);
C = drawCircle(0,0,1,3600,'b-','LineWidth',cw);
xC = get(C,'XData');
yC = get(C,'YData');
if(colgoutte)
   patch(xC,yC,[0.95 0.96 1]);
end
%
ii = angle*fa;
H = sin(ii);
for lambda = L
figure(f1);
devia = [];
ih = 0;
for h = H
ih = ih+1;
%
n = nwater(lambda);
col = [0.6 0.6 0.6];
if(colray)
   col = wavelength2rgb(lambda);
end
coln = [0 0 0];
%
i = angle(ih)*fa;
if((i < 0) & (atneg ==1))
   col = (col+4)/5;
   coln = (coln+4)/5;
end
r = asin(sin(i)/n);
[P1x,P1y] = pol2cart(pi-i,1);
[P1ex,P1ey] = pol2cart(pi-i,lnorm);
[P2x,P2y] = pol2cart(2*r-i,1);
[P2ex,P2ey] = pol2cart(2*r-i,lnorm);
[P3x,P3y] = pol2cart(4*r-i-pi,1);
[P3ex,P3ey] = pol2cart(4*r-i-pi,lnorm);
if(i < 0)
   [P4x,P4y] = pol2cart(6*r-i,1);
   [P4ex,P4ey] = pol2cart(6*r-i,lnorm);
end
if(drawnorm)
   plot([0 P1ex],[0 P1ey],':','Color',coln,'LineWidth',lw);
   plot([0 P2ex],[0 P2ey],':','Color',coln,'LineWidth',lw);
   plot([0 P3ex],[0 P3ey],':','Color',coln,'LineWidth',lw);
   if(i < 0)
      plot([0 P4ex],[0 P4ey],':','Color',coln,'LineWidth',lw);
   end
end
D = 4*r-2*i;
PSx = P3x;
PSy = P3y;
if(i < 0)
   D = D+pi+2*r;
   PSx = P4x;
   PSy = P4y;
end
devia(ih) = D;
PEx = -2;
PEy = PSy-(PSx+2)*tan(D);
%
plot([-2 P1x],[h P1y],'-','Color',col,'LineWidth',lw);
plot([P1x P2x],[P1y P2y],'-','Color',col,'LineWidth',lw);
plot([P2x P3x],[P2y P3y],'-','Color',col,'LineWidth',lw);
if(i < 0)
   plot([P3x P4x],[P3y P4y],'-','Color',col,'LineWidth',lw);
end
plot([PSx PEx],[PSy PEy],'-','Color',col,'LineWidth',lw);
%
end
figure(f2);
plot(angle,devia/fa,'Color',col);
end
%
figure(f1);
hold off
figure(f2);
hold off
%
figure(f1);
axis('square');
set(gca,'Box','on');
set(gca,'XLim',[-2 1.1]);
set(gca,'XTick',[]);
set(gca,'YLim',[-2 1.1]);
set(gca,'YTick',[]);
if(ax == 0)
 set(gca,'Visible','off');
end
%
print -dpng Fig_goutte
print -dpdf Fig_goutte
%
figure(f2);
set(gca,'Box','on');
set(gca,'XLim',[min(angle) max(angle)]);
set(gca,'YLim',[0 45]);
if(i < 0)
   set(gca,'YLim',[45 90]);
end
xlabel('Incidence / °');
ylabel('Déviation / °');
grid on;
%
print -dpng Fig_deviation
print -dpdf Fig_deviation
