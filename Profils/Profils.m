x0 = 1000.0;
xmin = 999.99;
xmax = 1000.01;
ymax = 600;
M = 16;
T = 296;
P = 10;
SBC = 0.09;
cutoff = 200;
step = 25;
%
Constantes;
%
m = M*1E-3/Na;
dxG = x0*sqrt(2*k*T/m/c/c);
xG = x0+[-cutoff*dxG:dxG/step:cutoff*dxG];
IG = 2/dxG*sqrt(log(2)/pi)*exp(-4*log(2)*((xG-x0)/dxG).^2);
%
dxL = SBC*P/760;
xL = x0+[-cutoff*dxL*2:dxL/step:cutoff*dxL*2];
IL = 2/pi/dxL*(1+(2*(xL-x0)/dxL).^2).^(-1);
%
xV = xG;
IV = conv(IG,IL,'same')*dxG/step;
%
plot([0 0],[0 max(IG)*2],'k:',xG-x0,IG, 'r-',xL-x0,IL,'g-',xV-x0,IV,'b-');
%
set(gca,'XLim',[xmin xmax]-x0);
set(gca,'YLim',[0 ymax]);
xlabel('(Nombre d''onde – 1000) / cm^{-1}');
ylabel('Intensité / Unité arbitraire');
%
l = legend('Centre de la raie','Élargissement Doppler (température)','Élargissement collisionnel (pression)','Profil résultant');
set(l,'Box','off','Location','NorthWest','FontSize',6.5),
%
print -dpng Fig_profils
