xmin = 380;
xmax = 780;
dx = xmax-xmin;
ymin = 0.501;
ymax = 2.499;
dy = ymax-ymin;
T = [3800:200:7400];
TRJ = 5000;
ML = 4000;
L = [0.1:0.1:ML];
n = length(T);
%
Constantes;
pkg load image;
%
figure('PaperPosition',[1 1 27.7 19],'PaperOrientation','landscape');
%
hold on
%
specV=wavelength2rgb(xmin:0.1:xmax);
lc = length(specV);
for i = 1:lc
  r = specV(1,i,1);
  g = specV(1,i,2);
  b = specV(1,i,2);
  if ((r+g+b == 0) && ((i < 380) || (i> 780)))
    specV(1,i,1) = 1;
    specV(1,i,2) = 1;
    specV(1,i,3) = 1;
  end
end
X = [xmin:dx/(lc-1):xmax];
Y = [ymin:dy/(lc-1):ymax];
im = image(X,Y,specV*0.5+0.5);
%
mxx = [];
mxy = [];
%
RJ = rayleighjeans(L*1E-9,TRJ)/1E14;
%
W = wien(L*1E-9,TRJ)/1E14;
%
for i = 1:n
%
Y = planck(L*1E-9,T(i))/1E14;
if(T(i) == TRJ)
   YRJ = Y;
end
ind = find(Y == max(Y));
L0 = L(ind);
col = wavelength2rgb(L0);
plot(L,Y,'Color',col,'LineWidth',2);
ind = find(Y == max(Y));
mxx(i) = 0.2014052353*h*c/k/T(i)/1E-9;
mxy(i) = 0.01405235273*pi*h*c*c./(mxx(i)*1E-9).^5/1E14;
text(mxx(i)+20,mxy(i),[num2str(T(i)) ' K'],'FontName','arial','FontSize',18,'Color','k');
%
end
%
plot(mxx,mxy,'r--','LineWidth',2);
plot(mxx,mxy,'.','MarkerSize',30,'Color','k');
%
text(110,2.92,'Ultraviolet','FontName','arial','FontSize',24,'FontWeight','bold','Color','k');
text(540,2.92,'Visible','FontName','arial','FontSize',24,'FontWeight','bold','Color','k');
text(1350,2.92,'Infrarouge','FontName','arial','FontSize',24,'FontWeight','bold','Color','k');
%
hold off
%
set(gca,'Box','on');
set(gca,'XLim',[0 2000]);
set(gca,'XTick',[0:500:2000]);
set(gca,'YLim',[0 3]);
set(gca,'YTick',[0:0.5:3]);
set(gca,'LineWidth',2);
set(gca,'FontName','arial','FontSize',24);
%
xlabel('Longueur d''onde / nm');
ylabel('Exitance / 10^{14}.W.m^{-2}.sr^{-1}.m^{-1}');
%
axb = axes('Position',[0.55 0.35 0.32 0.50]);
%
hold on
%
semilogy(L,RJ,'k--','LineWidth',2);
semilogy(L,W,'b--','LineWidth',2);
semilogy(L,YRJ,'r-','LineWidth',2);
%
text(150,0.55,'Planck (5000 K)','FontName','arial','FontSize',18,'Color','r');
text(1000,0.014,'Approximation de Wien','FontName','arial','FontSize',18,'Color','b');
text(1500,0.34,'Théorie classique de Rayleigh-Jeans','FontName','arial','FontSize',18,'Color','k');
%
set(gca,'Box','on');
set(gca,'XLim',[0 ML]);
set(gca,'YLim',[0.001 10]);
set(gca,'LineWidth',2);
set(gca,'FontName','arial','FontSize',18);
%
xlabel('Longueur d''onde / nm');
%
hold off
%
print -dpng FigCorpsNoirFR
