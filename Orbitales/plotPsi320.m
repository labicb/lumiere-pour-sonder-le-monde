sta = 100;
str = 40;
rmax = 12;
f = 50;
%
[t,p] = meshgrid([0:pi/sta:pi],[0:2*pi/sta:2*pi]);
x = sin(t).*cos(p);
y = sin(t).*sin(p);
z = cos(t);
%
r = [1/str:1/str:1]*rmax;
lr = length(r);
R = Rkl(3,2,r);
%
col = copper(str+1);
%
rect = [0 0 20 20];
figure('PaperPosition',rect,'PaperUnits','centimeters');
%
hold on
%
for ir = 1:lr
  disp(ir);
%
rc = Ylmcos(2,0,t,p);
psi = R(ir)*rc;
psi2 = psi.^2;
scatter3(r(ir).*x,r(ir).*y,r(ir).*z,f*psi2,'MarkerEdgeColor',col(ir,:),'Marker','.');
%
end
%
hold off
view(142.5,30);
xlabel('\itx');
ylabel('\ity');
zlabel('\itz');
colormap hot;
axis image;
axis square;
set(gca,'Box','on');
set(gca,'BoxStyle','full');
set(gca,'XLim',[-rmax rmax]);
set(gca,'YLim',[-rmax rmax]);
set(gca,'ZLim',[-rmax rmax]);
set(gca,'FontSize',24);
grid;
%
print -dpng Fig_Psi320
