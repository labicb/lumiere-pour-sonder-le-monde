[x,y] = meshgrid(-20:0.01:20,0:0.01:1);
%
z = (cos(x)).^2;
zero = zeros(size(z));
zero(1,1) = 1;
%
figure
%
pcolor(x,y,z);
%
colormap gray;
shading interp;
%
set(gca,'XLim',[-20 20]);
set(gca,'YLim',[0 1]);
set(gca,'XTick',[]);
set(gca,'YTick',[]);
%
print -dpng young_class
%
figure
hold on
%
mesh(x,y,zero);
%
view(0,90);
colormap gray;
%
set(gca,'XLim',[-20 20]);
set(gca,'YLim',[0 1]);
set(gca,'ZLim',[0 1]);
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'ZTick',[]);
%
for i = 1:1000
%
for j = 1:10
xp = rand*40-20;
yp = rand;
zp = 0;
pp = rand*(cos(xp)).^2;
if(pp > 0.2)
plot3(xp,yp,zp,'.','Color','w');
end
end
%
fname = fullfile('img', sprintf ('img%03i.png', i));
eval(['print -dpng ' fname]);
%
end
%
hold off
