mx = 15;
st = 0.01;
nphot = 10000;
%
pkg load geometry
%
cm = colormap(gray);
lcm = length(cm);
zerocm = zeros(lcm,1);
cm(:,2) = zerocm;
cm(:,3) = zerocm;
cm(lcm,:) = [0.5 0.5 0.5];
%
[x,y] = meshgrid(-mx:st:mx,-1:0.01:1);
%
[a,b,c] = cylinder([1 1],360);
polyc = circleToPolygon([0 0 0.9],360);
xc = polyc(:,1);
yc = polyc(:,2)/mx;
zc = 3*ones(size(xc));
%
z = (cos(x)).^2*(1-1/64);
ze = 2*ones(size(x));
indf = find((abs(y) <= 0.7) & (abs(x) >= 0.5) & (abs(x) <= 0.7));
ze(indf) = ze(indf)*NaN;
%
figure('PaperUnits','centimeters','PaperPosition',[0 0 10 15]);
hold on
%
pcolor(x,y,z);
plot3([0 0.6],[0 0],[3 2],'r-');
plot3([0 -0.6],[0 0],[3 2],'r-');
plot3([0.6 9],[0 0],[2 0],'r-');
plot3([-0.6 9],[0 0],[2 0],'r-');
surf(x,y,ze,ze);
surf(a,b/mx,c*0.2+2.9,0*c );
%
view(70,22);
colormap(cm);
caxis ([0 1]);
shading interp;
%
patch(xc,yc,zc,'k','FaceColor',[0 0 0]);
%
hold off
%
set(gca,'Box','on');
set(gca,'XLim',[-mx mx]);
set(gca,'YLim',[-1 1]);
set(gca,'ZLim',[0 3]);
set(gca,'Visible','off');
%
print -r600 -dpng FigYoung3D_wave
%
figure('PaperUnits','centimeters','PaperPosition',[0 0 10 15]);
hold on
%
pcolor(x,y,z*0);
zp = [2:0.1:2.9];
zerop = zeros(size(zp));
plot3(zerop,zerop,zp,'r.');
surf(x,y,ze,ze);
surf(a,b/mx,c*0.2+2.9,0*c );
%
%
for i = 1:nphot
xp = rand*2*mx-mx;
yp = rand*2-1;
zp = 0;
pp = rand*(cos(xp)).^2;
if(pp > 0.2)
plot3(xp,yp,zp,'.','Color','r','MarkerSize',0.1);
end
end
%
view(70,22);
colormap(cm);
caxis ([0 1]);
shading interp;
%
patch(xc,yc,zc,'k','FaceColor',[0 0 0]);
%
hold off
%
set(gca,'Box','on');
set(gca,'XLim',[-mx mx]);
set(gca,'YLim',[-1 1]);
set(gca,'ZLim',[0 3]);
set(gca,'Visible','off');
%
print -r600 -dpng FigYoung3D_photons