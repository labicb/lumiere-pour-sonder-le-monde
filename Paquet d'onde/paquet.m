col = 'cyan';
sz = 15;
l = 2;
w = 64;
%
x = [-6:0.01:6]*pi;
yg = exp(-x.^2/w);
y = sin(x).*yg;
%
figure('PaperUnits','centimeters','PaperPosition',[0 0 10 5]);
%
hold on
plot(x,y,'.','Color',col,'MarkerSize',sz);
plot(x,yg,'--','Color',col,'LineWidth',l);
plot(x,-yg,'--','Color',col,'LineWidth',l);
hold off
%
set(gca,'XLim',[-6 6]*pi);
set(gca,'YLim',[-1 1]*1.02);
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Visible','off');
%
imname = ['paquet_' col '.png'];
im = print(gcf,'-RGBImage','-r300');
tcolor = [255 255 255];
alpha = 255*(1-(im(:,:,1)==tcolor(1)).*(im(:,:,2)==tcolor(2)).*(im(:,:,3)==tcolor(3)));
imwrite(im, imname, 'Alpha', alpha);
