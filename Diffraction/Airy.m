mx = 4;
st = 0.005;
d = 0.0;
%
X = [-mx:st:mx];
[x,y] = meshgrid(X,X);
%
r = sqrt(x.^2+y.^2);
f = (besselj(1,pi*r)./(pi*r)).^2+d;
fl = f(floor(length(X)/2),:);
%
figure('PaperUnits','centimeters','PaperPosition',[0 0 15 15]);
%
hold on
%
pcolor(x,y,sqrt(f));
plot(X,fl*14,'r-','LineWidth',1);
%
hold off
%
cm = copper(2048);
colormap(cm);
shading interp;
%
axis square;
%
set(gca,'XLim',[-mx mx]);
set(gca,'YLim',[-mx mx]);
set(gca,'Visible','off');
%
print -dpng Fig_Airy
