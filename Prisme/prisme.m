drawnorm = 1;
ax = 0;
video = 0;
%
h = 0.6;
angle = 40;
%angle = [59:-0.1:0];
%L = 650;
L = [780:-2.5:380];
%
lw = 1;
cw = 1;
%
fa = pi/180;
%
pkg load geometry
pkg load image
%
figure('PaperUnits','centimeters','PaperPosition',[1 1 19 19]);
%
k = 0;
for p1 = angle
k = k+1;
%
i1 = (30+p1)*fa;
%
hold on
%
r32 = sqrt(3)/2;
base = (3-r32)/2;
xP = [1 2 1.5 1];
yP = [base base base+r32 base];
%
plot(xP,yP,'b-','LineWidth',cw);
patch(xP,yP,[0.95 0.96 1]);
%
for lambda = L
%
n = nglass(lambda);
i2 = asin(sin(i1)/n);
col = wavelength2rgb(lambda);
H = r32+h;
X = 1+h/tan(60*fa);
p2 = i2-pi/6;
%
if(drawnorm)
   plot([0.9 1.7],base+h+[(X-0.9) (X-1.7)]/sqrt(3),'k:','LineWidth',lw);
end
plot([0 X],[base+h-X*tan(p1*fa) base+h],'-','Color',col,'LineWidth',lw);
%
alpha = 2*pi/3-p2;
ll = (r32-h)/r32;
cc = ll*r32/sin(alpha);
aa = cc*cos(p2);
bb = cc*sin(p2);
%
plot([X X+aa],[base+h base+h+bb],'-','Color',col,'LineWidth',lw);
%
if(drawnorm)
   plot([1.3 2.1],base+h+bb-[(X+aa-1.3) (X+aa-2.1)]/sqrt(3),'k:','LineWidth',lw);
end


i3 = alpha-pi/2;
if(n*sin(i3) <= 1)
   i4 = asin(sin(i3)*n);
   p3 = pi/6-i4;
   plot([X+aa 3],[base+h+bb base+h+bb+(3-X-aa)*tan(p3)],'-','Color',col,'LineWidth',lw);
end
%
end
%
hold off
%
axis('square');
set(gca,'Box','on');
set(gca,'XLim',[0 3]);
set(gca,'XTick',[]);
set(gca,'YLim',[0 3]);
set(gca,'YTick',[]);
if(ax == 0)
 set(gca,'Visible','off');
end
%
if(video)
   fname = fullfile('img', sprintf ('img%03i.png', k));
   eval(['print -dpng ' fname]);
   clf;
end
%
end
%
if(video == 0)
   print -dpng -r1200 Fig_prisme
   print -dpdf Fig_prisme
end
