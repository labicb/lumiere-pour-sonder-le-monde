function n = nglass(lambda)
%
% Verre Flint.
%
n=1.619+10.20E3/lambda^2;
%
end
