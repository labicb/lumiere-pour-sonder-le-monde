pkg load geometry;
%
tmax = 5;
v = 0.8;
x = [-5:0.01:5];
y = 3*sin(4*x);
yb = 3*sin(4*2/v*x);
yr = 3*sin(4*v*x);
%
figure('PaperUnits','centimeters','PaperPosition',[0 0 40 23]/2);
%
hold on
%
plot(0,tmax+1,'k.');
plot(2*tmax*v,-tmax-1,'k.');
plot(0,-tmax-1,'ko');
%
for t = 1:tmax
%
drawCircle(0,tmax+1,t,3600,'k-');
drawCircle((2*tmax-t)*v,-tmax-1,t,3600,'k-');
%
end
%
plot(x+10,y+tmax+1,'g-');
plot(x-10,y+tmax+1,'g-');
plot(x+10+tmax*v,yb-tmax-1,'b-');
plot(x-2.5-tmax*v,yr-tmax-1,'r-');
%
hold off
%
set(gca,'Visible','off');
set(gca,'XLim',[-3.5*tmax 4.5*tmax]);
set(gca,'YLim',[-2.3*tmax 2.3*tmax]);
axis equal;
%
print -dpng Fig_Doppler
