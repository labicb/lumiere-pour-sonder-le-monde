M = 4;
N = 3600;
%
pkg load geometry
%
[a,b,c] = cylinder([1 1],N);
%
xc = a+2;
yc = c*2-1;
zc = b+2;
%
polyc = circleToPolygon([2 2 1],N);
xci1 = polyc(:,1);
yci1 = ones(size(xci1));
zci1 = polyc(:,2);
xci2 = xci1;
yci2 = yci1-2;
zci2 = zci1;
xci3 = xci1;
yci3 = yci1+0.999;
zci3 = zci1;
%
xr = [M M M M M]*0.995;
yr = [-1 -1 1 1 -1];
zr = [1 3 3 1 1];
%
xa1 = [0 M M 0 0];
ya1 = ones(size(xa1))*2;
za1 = [0 0 M M 0];
%
xa2 = [0 M M 0 0];
ya2 = [-M -M M M -M]/2;
za2 = zeros(size(xa1));
%
xa3 = ones(size(xa1))*M;
ya3 = [-M -M M M -M]/2;
za3 = [0 M M 0 0];
%
figure;
hold on;
%
surf(xc,yc,zc,xc);
shading interp;
colormap hot;
patch(xci1,yci1,zci1,'r','LineWidth',0.1);
patch(xci2,yci2,zci2,'r','LineWidth',0.1);
%
patch(xa1,ya1,za1,'k','FaceColor',[0.9 0.9 0.9]);
patch(xa2,ya2,za2,'k','FaceColor',[0.9 0.9 0.9]);
patch(xa3,ya3,za3,'k','FaceColor',[0.9 0.9 0.9]);
%
patch(xci3,yci3,zci3,'r','FaceColor',[1 0.8 0.8],'LineWidth',0.1);
patch(xr,yr,zr,'r','FaceColor',[1 1 0.8],'LineWidth',0.1);
%
hold off;
view(-55,30);
%
%xlabel('\itx');
%ylabel('\ity');
%zlabel('\itz');
%
set(gca,'Box','on');
set(gca,'BoxStyle','full');
set(gca,'XLim',[0 M]);
set(gca,'YLim',[-2 2]);
set(gca,'ZLim',[0 M]);
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'ZTick',[]);
axis square;
%
print -r600 -dpng Fig_Cylindre
