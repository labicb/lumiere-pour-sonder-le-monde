lw = 1;
%
load HNIST.xy;
x = HNIST(:,1);
l = length(x);
%
pkg load image
%
figure('PaperUnits','centimeters','PaperPosition',[0 0 15 2]);;
hold on
%
for i = 1:l
  c = wavelength2rgb(x(i));
  plot([x(i) x(i)],[0 1],'Color',c,'LineWidth',lw);
end
%
set(gca,'Box','on');
set(gca,'Color',[0 0 0]);
set(gca,'XLim',[400 700]);
set(gca,'XTick',[]);
set(gca,'XColor',[0 0 0]);
set(gca,'YLim',[0 1]);
set(gca,'YTick',[]);
set(gca,'YColor',[0 0 0]);
%
set(gcf,'Color',[0 0 0]);
set(gcf,'InvertHardCopy','off')
%
print -r600 -dpng Fig_H_color
