z = [0:pi/5000:7*pi];
zero = zeros(size(z));
EB = cos(z);
%
f = figure('PaperUnits','centimeters','PaperOrientation','landscape','PaperPosition',[1 1 27.7 19]*4);
%
hold on
daspect([7 1 1])
plot3(z,-EB,zero,'b-',z,zero,EB,'r-','LineWidth',4);
plot3([2*pi 2*pi],[0 0],[1 1.4],'k--','LineWidth',2);
plot3([4*pi 4*pi],[0 0],[1 1.4],'k--','LineWidth',2);
arrow3([0 0 0],[0 -1 0],'b1');
plot3([0 0],[0 -0.9],[0 0],'b-','LineWidth',4);
arrow3([0 0 0],[0 0 1],'r1');
plot3([0 0],[0 0],[0 0.9],'r-','LineWidth',4);
arrow3([0 0 0],[4 0 0],'k1');
plot3([0 3.9],[0 0],[0 0],'k-','LineWidth',4);
arrow3([2*pi 0 1.4],[4*pi 0 1.4],'k1');
plot3([2*pi+0.1 4*pi-0.1],[0 0],[1.4 1.4],'k-','LineWidth',4);
arrow3([4*pi 0 1.4],[2*pi 0 1.4],'k1');
text(3*pi,0,1.5,'\lambda','Fontsize',48);
text(-1.5,-1,0,'{\itE}','Color','b','Fontsize',48);
text(-1.5,-1,0.1,'{\rightarrow}','Color','b','Fontsize',48);
text(-1.5,0,1,'{\itB}','Color','r','Fontsize',48);
text(-1.5,0,1.1,'{\rightarrow}','Color','r','Fontsize',48);
text(4.2,-0.1,0.125,'{\itc}','Color','k','Fontsize',48);
text(4.2,-0.1,0.2,'{\rightarrow}','Color','k','Fontsize',48);
text(5.5*pi,0,1.2,'\lambda longueur d''onde','Fontsize',48);
text(5.5*pi,0,1.4,'{\itc} célérité','Fontsize',48);
text(5.5*pi,0,1.475,'\rightarrow','Fontsize',48);
text(5.5*pi,0,1.6,'{\itB} champ magnétique','Color','r','Fontsize',48);
text(5.5*pi,0,1.675,'\rightarrow','Color','r','Fontsize',48);
text(5.5*pi,0,1.8,'{\itE} champ électrique','Color','b','Fontsize',48);
text(5.5*pi,0,1.875,'\rightarrow','Color','b','Fontsize',48);
pE = patch([0 7*pi 7*pi 0 0],[1 1 -1 -1 1],[0 0 0 0 0],[0.8 0.8 1]);
alpha(pE,0.3);
pB = patch([0 7*pi 7*pi 0 0],[0 0 0 0 0],[1 1 -1 -1 1],[1 0.8 0.8]);
alpha(pB,0.3);
hold off
%
hAxis = gca;
hAxis.XRuler.FirstCrossoverValue  = 0; % X crossover with Y axis
hAxis.XRuler.SecondCrossoverValue  = 0; % X crossover with Z axis
hAxis.YRuler.FirstCrossoverValue  = 0; % Y crossover with X axis
hAxis.YRuler.SecondCrossoverValue  = 0; % Y crossover with Z axis
hAxis.ZRuler.FirstCrossoverValue  = 0; % Z crossover with X axis
hAxis.ZRuler.SecondCrossoverValue = 0; % Z crossover with Y axis
set(gca,'YLim',[-1.5 1.5])
set(gca,'ZLim',[-1.5 1.5])
set(gca,'XTick',[])
set(gca,'YTick',[])
set(gca,'ZTick',[])
view([30 20])
%
print -dpng FigEM
